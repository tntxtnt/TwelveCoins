#include "mymath.h"


Combination::Combination(int n, int k)
: n(n), k(k), data(k)
{
    std::iota(begin(data), end(data), 0);
}

bool Combination::next_combination()
{
    if (n - k == data[0]) return false;
    int i = k - 1;
    while (i > 0 && data[i] == n-k+i) --i;
    ++data[i];
    for (int j = i; j < k-1; ++j) data[j+1] = data[j] + 1;
    return true;
}