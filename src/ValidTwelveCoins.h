#ifndef VALIDTWELVECOINS_H
#define VALIDTWELVECOINS_H

#include "Balance.h"
#include <vector>
#include <map>

using WeighRes           = std::map<std::string,int>;
using WeighTriplet       = std::array<CoinSet,3>;
using ThreeWeighTriplets = std::array<WeighTriplet,3>;

bool valid(const ThreeWeighPairs&, const WeighRes&);
bool valid(const ThreeWeighPairs&);
ThreeWeighTriplets toThreeWeighTriplets(const ThreeWeighPairs&);
std::vector<int> intersections(const WeighTriplet&);
int fakeCoinId(const ThreeWeighPairs&, const TwelveCoins&);

#endif