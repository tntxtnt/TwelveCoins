#ifndef TWELVECOINS_H
#define TWELVECOINS_H

class Balance;

class TwelveCoins
{
    friend class Balance;
public:
    TwelveCoins(int fid=1, bool isFakeHeavier=0)
    : fid(fid), isFakeHeavier(isFakeHeavier) {}
private:
    int fid; //fake coin's index
    bool isFakeHeavier;
};

#endif // TWELVECOINS_H