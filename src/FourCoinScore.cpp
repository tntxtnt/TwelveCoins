#include "FourCoinScore.h"

TCScore score(const CoinSet& cs)
{
    std::vector<int> arr(begin(cs), end(cs));
    int m = arr.back() - arr.front(); //muy (average)
    int s = 0; //sigma square (variance)
    for (int i = 0; i < 3; ++i)
    {
        int diff = 3*(arr[i+1]-arr[i]) - m;
        s += diff * diff;
    }
    return {s,m,arr[0]};
}

bool fourCoinScoreComp(const CoinSet& lhs, const CoinSet& rhs)
{
    auto s1 = score(lhs);
    auto s2 = score(rhs);
    return std::lexicographical_compare(begin(s1), end(s1),
                                        begin(s2), end(s2));
}

ThreeWeighPairsScore score(const ThreeWeighPairs& wp3)
{
    ThreeWeighPairsScore result;
    std::vector<TCScore> sixScores;
    for (auto const& wp : wp3)
    {
        sixScores.push_back(score(wp.first));
        sixScores.push_back(score(wp.second));
    }
    std::sort(begin(sixScores), end(sixScores));
    for (int i = 0; i < 3; ++i)
        for (int j = 0; j < 6; ++j)
            result[i][j] = sixScores[j][i];
    return result;
}
