#include <iostream>
#include <iomanip>
#include "ValidTwelveCoins.h"
#include <cassert>
#include "ValidConfig.h"
#include "FourCoinScore.h"
#include "mymath.h"
#include <queue>

template <class Vec, class IntIter>
Vec applyPermutation(const Vec&, IntIter, IntIter);

ThreeWeighPairs configTo3WeighPairs(const TCConfig&);

std::ostream& operator<<(std::ostream&, const ThreeWeighPairs&);

int main()
{
    // ValidTwelveCoins
    ThreeWeighPairs wpairs {
        WeighPair{{4,5,6,7}, {8,9,10,11}},
        WeighPair{{6,7,8,9}, {1,2,11,12}},
        WeighPair{{2,4,6,8}, {1,3,5,7}}
    };
    WeighRes wres {
        {"LLL", 6}, {"RRR", 6},
        {"LLB",11}, {"RRB",11},
        {"LLR", 7}, {"RRL", 7},
        {"LBL", 4}, {"RBR", 4},
        {"LBB",10}, {"RBB",10},
        {"LBR", 5}, {"RBL", 5},
        {"LRL", 0}, {"RLR", 0},
        {"LRB", 9}, {"RLB", 9},
        {"LRR", 8}, {"RLL", 8},
        {"BLL", 1}, {"BRR", 1},
        {"BLB",12}, {"BRB",12},
        {"BLR", 2}, {"BRL", 2},
        {"BBL", 3}, {"BBR", 3},
        {"BBB",13},
    };
    if (valid(wpairs, wres)) std::cout << "All 26 tests PASSED.\n";
    if (valid(wpairs)) std::cout << "All 26 tests PASSED.\n";


    // ValidConfig
    auto vConfigs = getValidConfigs();
    for (auto& cf : vConfigs) std::sort(begin(cf), end(cf), LBRcomps);
    std::sort(begin(vConfigs), end(vConfigs), LBRcompc);
    //123 132 213 231 312 321
    std::map<TCConfig,int> configCount;
    int pcount = 0;
    int pvalid = 0;
    for (auto const& cf : vConfigs)
    {
        int id[3] = {0,1,2};
        do {
            auto ccf = cf;
            for (auto& s : ccf) s = applyPermutation(s, id, id+3);
            std::sort(begin(ccf), end(ccf), LBRcomps);
            auto it = std::find(begin(vConfigs), end(vConfigs), ccf);
            if (it != vConfigs.end()) pvalid++;
            pcount++;
            configCount[ccf]++;
        } while (std::next_permutation(id, id+3));
    }
    std::cout << pcount << "\n" << pvalid << "\n";
    /*for (auto const& kv : configCount)
        std::cout << kv.first << ": " << kv.second << "\n";*/


    //FourCoinScore
    Combination c_12_4(12, 4);
    std::vector<CoinSet> cs495;
    do {
        CoinSet cs;
        for (int i : c_12_4.array()) cs.insert(i+1);
        cs495.push_back(cs);
    } while (c_12_4.next_combination());

    std::sort(begin(cs495), end(cs495), fourCoinScoreComp);
    /*for (auto& cs : cs495)
    {
        for (auto& i : cs) std::cout << std::setw(2) << i << " ";
        std::cout << "\n";
    }*/


    //ThreeWeighPairs scores
    TCConfig testConfig = {
        "LLL","LBR","LRL","LRB","BLL","BLB",
        "BLR","BBR","RBB","RBR","RRL","RRB"
    };
    int p12[] = {0,1,2,3,4,5,6,7,8,9,10,11,12};
    int count = 0;
    std::priority_queue<TWP_TWPS, std::vector<TWP_TWPS>,
                        TWP_TWPS_VarianceComp> best100;
    do {
        auto permutConfig = applyPermutation(testConfig, p12, p12+12);
        TWP_TWPS wp3s;
        wp3s.first = configTo3WeighPairs(permutConfig);
        wp3s.second = score(wp3s.first);
        best100.push(wp3s);
        if (best100.size() > 100) best100.pop();
        count++;
        if (count%100000 == 0) std::cout << count/100000 << "e5\r";
    } while (std::next_permutation(p12, p12+9)); //12 ~ 90 min!

    std::cout << "\n\n";
    for (; !best100.empty(); best100.pop())
        std::cout << best100.top().first << "\n";
}


template <class Vec, class IntIter>
Vec applyPermutation(const Vec& src, IntIter beg, IntIter end)
{
    Vec v;
    std::transform(beg, end, std::back_inserter(v), [&src](int i){
        return src[i];
    });
    return v;
}

ThreeWeighPairs configTo3WeighPairs(const TCConfig& cf)
{
    ThreeWeighPairs result;
    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 12; ++j)
        {
            if (cf[j][i] == 'L') result[i].first.insert(j+1);
            if (cf[j][i] == 'R') result[i].second.insert(j+1);
        }
    }
    return result;
}

std::ostream& operator<<(std::ostream& out, const ThreeWeighPairs& wp3)
{
    for (auto& wp : wp3)
    {
        for (int i : wp.first) out << i << "-";
        out << "\b ; ";
        for (int i : wp.second) out << i << "-";
        out << "\b \n";
    }
    return out;
}