#ifndef BALANCE_H
#define BALANCE_H

#include "TwelveCoins.h"
#include <set>
#include <algorithm>
#include <string>
#include <array>

using CoinSet         = std::set<int>;
using WeighPair       = std::pair<CoinSet,CoinSet>;
using ThreeWeighPairs = std::array<WeighPair,3>;

class Balance
{
public:
    enum {L,B,R};
    static char weigh(const TwelveCoins&, const WeighPair&);
    static std::string weigh(const TwelveCoins&, const ThreeWeighPairs&);
};

#endif // BALANCE_H