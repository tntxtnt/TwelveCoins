#include "ValidConfig.h"

bool isValidConfig(const TCConfig& config)
{
    std::map<char,int> cmap[3];
    for (auto const& wres : config)
        for (int i = 0; i < 3; ++i)
            cmap[i][wres[i]]++;
    for (int i = 0; i < 3; ++i)
        if (cmap[i]['L'] != cmap[i]['B'] || cmap[i]['L'] != cmap[i]['R'])
            return false;
    return true;
}

std::string counter(std::string s)
{
    for (auto& c : s) if (c == 'L') c = 'R'; else if (c == 'R') c = 'L';
    return s;
}

std::vector<TCConfig> getValidConfigs()
{
    std::vector<TCConfig> result;
    TCConfig initialConfigs[] = {
        {"LLB","LLR","LBL","LBB","LBR","LRL",
         "LRB","LRR","BLL","BLB","BLR","BBL"},
        {"LLL","LLB","LBL","LBB","LBR","LRL",
         "LRB","LRR","BLL","BLB","BLR","BBL"},
        {"LLL","LLB","LLR","LBL","LBB","LBR",
         "LRB","LRR","BLL","BLB","BLR","BBL"},
        {"LLL","LLB","LLR","LBL","LBB","LBR",
         "LRL","LRB","BLL","BLB","BLR","BBL"}
    };
    for (auto const& startConfig : initialConfigs)
        for (int n = 1<<startConfig.size(); n-->0; )
        {
            TCConfig config = startConfig;
            for (int i = startConfig.size(), mask = 1; i-->0; mask<<=1)
                if (mask&n) config[i] = counter(config[i]);
            if (isValidConfig(config)) result.push_back(config);
        }
    return result;
}

std::ostream& operator<<(std::ostream& out, const TCConfig& config)
{
    for (auto& s : config) out << s << " ";
    return out;
}

int LBRval(char c)
{
    switch (c) {
        case 'L': return 0;
        case 'B': return 1;
        case 'R': return 2;
        default : return -1;
    }
}

bool LBRcomps(const std::string& lhs, const std::string& rhs)
{
    int n = std::min(lhs.size(), rhs.size());
    for (int i = 0; i < n; ++i)
        if (LBRval(lhs[i]) < LBRval(rhs[i])) return true;
        else if (LBRval(lhs[i]) > LBRval(rhs[i])) return false;
    return lhs.size() < rhs.size();
}

bool LBRcompc(const TCConfig& lhs, const TCConfig& rhs)
{
    for (size_t i = 0; i < lhs.size(); ++i)
    {
        if (lhs[i] == rhs[i]) continue;
        return LBRcomps(lhs[i], rhs[i]);
    }
    return false;
}