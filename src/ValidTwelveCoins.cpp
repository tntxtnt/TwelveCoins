#include "ValidTwelveCoins.h"


bool valid(const ThreeWeighPairs& wpairs, const WeighRes& wres)
{
    for (int i = 1; i <= 13; ++i)
        if ( i != wres.at(Balance::weigh(TwelveCoins(i,0), wpairs))
          || i != wres.at(Balance::weigh(TwelveCoins(i,1), wpairs)))
            return false;
    return true;
}

bool valid(const ThreeWeighPairs& wpairs)
{
    for (int i = 1; i <= 13; ++i)
        if ( i != fakeCoinId(wpairs, TwelveCoins(i,0))
          || i != fakeCoinId(wpairs, TwelveCoins(i,1)) )
            return false;
    return true;
}

ThreeWeighTriplets toThreeWeighTriplets(const ThreeWeighPairs& wpairs)
{
    static const CoinSet fullSet = {1,2,3,4,5,6,7,8,9,10,11,12,13};
    ThreeWeighTriplets ret{
        //                  L             B             R
        WeighTriplet{wpairs[0].first, CoinSet{}, wpairs[0].second},
        WeighTriplet{wpairs[1].first, CoinSet{}, wpairs[1].second},
        WeighTriplet{wpairs[2].first, CoinSet{}, wpairs[2].second}
    };
    for (auto& wt : ret) //find coins not in weighing
        for (int i = 1; i <= 13; ++i)
            if ( wt[Balance::L].find(i) == end(wt[Balance::L])
              && wt[Balance::R].find(i) == end(wt[Balance::R]) )
                wt[Balance::B].insert(i);
    return ret;
}

std::vector<int> intersections(const WeighTriplet& p)
{
    std::vector<int> tmp, ret;
    std::set_intersection(begin(p[0]), end(p[0]), begin(p[1]), end(p[1]),
                          std::back_inserter(tmp));
    std::set_intersection(begin(tmp), end(tmp), begin(p[2]), end(p[2]),
                          std::back_inserter(ret));
    return ret;
}

int fakeCoinId(const ThreeWeighPairs& wpairs, const TwelveCoins& coins)
{
    auto wres = Balance::weigh(coins, wpairs);
    auto wtriplets = toThreeWeighTriplets(wpairs);
    WeighTriplet p;
    std::vector<int> in;
    for (int i = 0; i < 3; ++i)
    {   // assume heavier
        if      (wres[i] == 'L') p[i] = wtriplets[i][Balance::L];
        else if (wres[i] == 'R') p[i] = wtriplets[i][Balance::R];
        else                     p[i] = wtriplets[i][Balance::B];
    }
    if ((in = intersections(p)).empty()) // lighter
    {
        for (int i = 0; i < 3; ++i)
        {
            if      (wres[i] == 'L') p[i] = wtriplets[i][Balance::R];
            else if (wres[i] == 'R') p[i] = wtriplets[i][Balance::L];
        }
        in = intersections(p);
    }
    return in[0];
}