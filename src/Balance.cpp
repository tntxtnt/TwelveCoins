#include "Balance.h"

char Balance::weigh(const TwelveCoins& cs, const WeighPair& wpair)
{
    auto const& lhs = wpair.first;
    auto const& rhs = wpair.second;
    if (lhs.size() != rhs.size())     return lhs.size()<rhs.size() ? 'R' : 'L';
    if (lhs.find(cs.fid) != end(lhs)) return cs.isFakeHeavier      ? 'L' : 'R';
    if (rhs.find(cs.fid) != end(rhs)) return cs.isFakeHeavier      ? 'R' : 'L';
    return 'B';
}

std::string Balance::weigh(const TwelveCoins& cs, const ThreeWeighPairs& wps)
{
    std::string ret;
    std::transform(begin(wps), end(wps), std::back_inserter(ret),
        [&cs](const WeighPair& wp){ return weigh(cs, wp); });
    return ret;
}