#ifndef VALIDCONFIG_H
#define VALIDCONFIG_H

#include <vector>
#include <string>
#include <map>
#include <iostream>
#include <algorithm>

using TCConfig = std::vector<std::string>;

bool isValidConfig(const TCConfig&);
std::vector<TCConfig> getValidConfigs();
std::ostream& operator<<(std::ostream&, const TCConfig&);
bool LBRcomps(const std::string&, const std::string&);
bool LBRcompc(const TCConfig&, const TCConfig&);

#endif