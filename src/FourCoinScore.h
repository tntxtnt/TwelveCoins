#ifndef FOURCOINSCORE_H
#define FOURCOINSCORE_H

#include "Balance.h"
#include <iostream>
#include <vector>

using TCScore = std::array<int,3>;
using ThreeWeighPairsScore = std::array<std::array<int,6>,3>;
using TWP_TWPS = std::pair<ThreeWeighPairs,ThreeWeighPairsScore>;

TCScore score(const CoinSet&);
bool fourCoinScoreComp(const CoinSet&, const CoinSet&);
ThreeWeighPairsScore score(const ThreeWeighPairs&);


struct TWP_TWPS_VarianceComp {
    bool operator()(const TWP_TWPS& lhs,
                    const TWP_TWPS& rhs)const
    {
        auto& s1 = lhs.second;
        auto& s2 = rhs.second;
        if (s1[0] != s2[0]) return std::lexicographical_compare(
            begin(s1[0]), end(s1[0]), begin(s2[0]), end(s2[0])
        );
        if (s1[1] != s2[1]) return std::lexicographical_compare(
            begin(s1[1]), end(s1[1]), begin(s2[1]), end(s2[1])
        );
        return std::lexicographical_compare(
            begin(s1[2]), end(s1[2]), begin(s2[2]), end(s2[2])
        );
    }
};

#endif