#ifndef MYMATH_H
#define MYMATH_H

#include <vector>
#include <algorithm>
#include <numeric>
using IntVec = std::vector<int>;

class Combination
{
public:
    Combination(int, int);
    bool next_combination();
    const IntVec& array()const { return data; }
private:
    int n, k;
    IntVec data;
};

#endif