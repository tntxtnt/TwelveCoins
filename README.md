### Description
Find easy to remember ways to find fake coin in a set of 12 coins in less than 3 (balance) weighs.

Algorithm: sadly, brute-force. It takes about 90 mins to find the best permutation in `12!` permutations. The result (best 100, the best way is the last one) of the 107th config in total of 304 configs is included in `107-best100.txt` file.

### Compilation
Pretty straightforward: run `make build` to build the project, and `make run` to run the executatble. Tested on Windows.

### License
Copyright (c) 2016 Tri Tran. Distributed under MIT license.